# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Jason Collins <JasonPCollins@protonmail.com>, 2019
# Jeff Bailes <thepizzaking@gmail.com>, 2007
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-17 06:31+0200\n"
"PO-Revision-Date: 2019-10-25 15:41+0000\n"
"Last-Translator: Jason Collins <JasonPCollins@protonmail.com>\n"
"Language-Team: English (United Kingdom) (http://www.transifex.com/xfce/xfce-apps/language/en_GB/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en_GB\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../xfburn/xfburn-global.h:28
msgid "Capacity:"
msgstr "Capacity:"

#: ../xfburn/xfburn-global.h:29
msgid "addr:"
msgstr "addr:"

#: ../xfburn/xfburn-global.h:30
msgid "Time total:"
msgstr "Time total:"

#: ../xfburn/xfburn-global.h:32
msgid "length"
msgstr "length"

#: ../xfburn/xfburn-global.h:33 ../xfburn/xfburn-copy-cd-progress-dialog.c:93
msgid "Flushing cache..."
msgstr "Flushing cache..."

#: ../xfburn/xfburn-global.h:34
msgid "Please insert a recordable disc and hit enter"
msgstr "Please insert a recordable disc and hit enter"

#: ../xfburn/xfburn-global.h:35
msgid "Cannot determine disc status - hit enter to try again."
msgstr "Cannot determine disc status - hit enter to try again."

#: ../xfburn/xfburn-global.h:36
msgid "CD copying finished successfully."
msgstr "CD copying finished successfully."

#: ../xfburn/xfburn-global.h:43
msgid "Data composition"
msgstr "Data composition"

#: ../xfburn/xfburn-adding-progress.c:83
msgid "Adding files to the composition"
msgstr "Adding files to the composition"

#: ../xfburn/xfburn-adding-progress.c:96
msgid "Cancel"
msgstr "Cancel"

#: ../xfburn/xfburn-blank-dialog.c:70
msgid "Quick Blank"
msgstr "Quick Blank"

#: ../xfburn/xfburn-blank-dialog.c:71
msgid "Full Blank (slow)"
msgstr "Full Blank (slow)"

#: ../xfburn/xfburn-blank-dialog.c:72
msgid "Quick Format"
msgstr "Quick Format"

#: ../xfburn/xfburn-blank-dialog.c:73
msgid "Full Format"
msgstr "Full Format"

#: ../xfburn/xfburn-blank-dialog.c:74
msgid "Quick Deformat"
msgstr "Quick Deformat"

#: ../xfburn/xfburn-blank-dialog.c:75
msgid "Full Deformat (slow)"
msgstr "Full Deformat (slow)"

#: ../xfburn/xfburn-blank-dialog.c:119
msgid "Eject the disc"
msgstr "Eject the disc"

#: ../xfburn/xfburn-blank-dialog.c:120
msgid "Default value for eject checkbox"
msgstr "Default value for eject tickbox"

#: ../xfburn/xfburn-blank-dialog.c:168
msgid "Blank Disc"
msgstr "Blank Disc"

#: ../xfburn/xfburn-blank-dialog.c:182
#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:165
#: ../xfburn/xfburn-burn-image-dialog.c:181
#: ../xfburn/xfburn-copy-cd-dialog.c:107
#: ../xfburn/xfburn-copy-dvd-dialog.c:104
#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:129
msgid "Burning device"
msgstr "Burning device"

#: ../xfburn/xfburn-blank-dialog.c:195 ../xfburn/xfburn-device-box.c:156
msgid "Blank mode"
msgstr "Blank mode"

#: ../xfburn/xfburn-blank-dialog.c:203
#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:197
#: ../xfburn/xfburn-burn-image-dialog.c:189
#: ../xfburn/xfburn-copy-cd-dialog.c:115
#: ../xfburn/xfburn-copy-dvd-dialog.c:112
#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:163
msgid "Options"
msgstr "Options"

#: ../xfburn/xfburn-blank-dialog.c:207
#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:201
#: ../xfburn/xfburn-burn-image-dialog.c:193
#: ../xfburn/xfburn-copy-cd-dialog.c:119
#: ../xfburn/xfburn-copy-dvd-dialog.c:116
#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:167
msgid "E_ject disk"
msgstr "E_ject disc"

#: ../xfburn/xfburn-blank-dialog.c:217
msgid "_Blank"
msgstr "_Blank"

#. blanking can only be performed on blank discs, format and deformat are
#. allowed to be blank ones
#: ../xfburn/xfburn-blank-dialog.c:329
msgid "The inserted disc is already blank."
msgstr "The inserted disc is already blank."

#. these ones we can blank
#: ../xfburn/xfburn-blank-dialog.c:335
msgid "Ready"
msgstr "Ready"

#: ../xfburn/xfburn-blank-dialog.c:338
msgid "No disc detected in the drive."
msgstr "No disc detected in the drive."

#: ../xfburn/xfburn-blank-dialog.c:347
msgid "Disc is not erasable."
msgstr "Disc is not erasable."

#: ../xfburn/xfburn-blank-dialog.c:387
msgid "Blanking disc..."
msgstr "Blanking disc..."

#: ../xfburn/xfburn-blank-dialog.c:414
#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:486
#: ../xfburn/xfburn-perform-burn.c:489
#, c-format
msgid "Done"
msgstr "Done"

#: ../xfburn/xfburn-blank-dialog.c:417 ../xfburn/xfburn-perform-burn.c:496
#: ../xfburn/xfburn-progress-dialog.c:637
msgid "Failure"
msgstr "Failure"

#: ../xfburn/xfburn-blank-dialog.c:434
#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:592
#: ../xfburn/xfburn-burn-image-dialog.c:392
#: ../xfburn/xfburn-burn-image-dialog.c:570
#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:365
msgid "Unable to grab the drive."
msgstr "Unable to grab the drive."

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:128
msgid "Image"
msgstr "Image"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:130
msgid "Show volume name"
msgstr "Show volume name"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:130
msgid "Show a text entry for the name of the volume"
msgstr "Show a text entry for the name of the volume"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:151
#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:117
msgid "Burn Composition"
msgstr "Burn Composition"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:176
msgid "Composition name"
msgstr "Composition name"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:181
msgid "<small>Would you like to change the default composition name?</small>"
msgstr "<small>Would you like to change the default composition name?</small>"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:206
#: ../xfburn/xfburn-burn-image-dialog.c:198
#: ../xfburn/xfburn-copy-cd-dialog.c:124
#: ../xfburn/xfburn-copy-dvd-dialog.c:121
#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:172
msgid "_Dummy write"
msgstr "_Dummy write"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:210
#: ../xfburn/xfburn-burn-image-dialog.c:202
#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:176
msgid "Burn_Free"
msgstr "Burn_Free"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:215
#: ../xfburn/xfburn-burn-image-dialog.c:207
msgid "Stream _Recording"
msgstr "Stream _Recording"

#. create ISO ?
#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:221
#: ../xfburn/xfburn-copy-cd-dialog.c:133
#: ../xfburn/xfburn-copy-dvd-dialog.c:130
msgid "Only create _ISO"
msgstr "Only create _ISO"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:258
#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:192
msgid "_Burn Composition"
msgstr "_Burn Composition"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:444
#, c-format
msgid "Could not create destination ISO file: %s"
msgstr "Could not create destination ISO file: %s"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:451
msgid "Writing ISO..."
msgstr "Writing ISO..."

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:468
#, c-format
msgid "An error occured while writing ISO: %s"
msgstr "An error occured while writing ISO: %s"

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:524
#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:530
msgid "An error occurred in the burn backend."
msgstr "An error occurred in the burn backend."

#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:553
#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:296
msgid "The write mode is not supported currently."
msgstr "The write mode is not supported currently."

#. could not create source
#: ../xfburn/xfburn-burn-data-composition-base-dialog.c:636
msgid "Could not create ISO source structure."
msgstr "Could not create ISO source structure."

#: ../xfburn/xfburn-burn-image-dialog.c:137
msgid "Burn image"
msgstr "Burn image"

#. file
#: ../xfburn/xfburn-burn-image-dialog.c:145
#: ../xfburn/xfburn-burn-image-dialog.c:166
msgid "Image to burn"
msgstr "Image to burn"

#: ../xfburn/xfburn-burn-image-dialog.c:149
msgid "All files"
msgstr "All files"

#: ../xfburn/xfburn-burn-image-dialog.c:153
msgid "ISO images"
msgstr "ISO images"

#: ../xfburn/xfburn-burn-image-dialog.c:212
msgid "_Quit after success"
msgstr "_Quit after success"

#. action buttons
#. align = gtk_alignment_new (0, 0, 0, 0);
#. gtk_alignment_set_padding (GTK_ALIGNMENT (align), 0, 0, BORDER * 4, 0);
#. gtk_widget_show (align);
#. gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
#. action buttons
#: ../xfburn/xfburn-burn-image-dialog.c:218
#: ../xfburn/xfburn-copy-cd-dialog.c:165
#: ../xfburn/xfburn-copy-dvd-dialog.c:162
#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:188
msgid "_Cancel"
msgstr "_Cancel"

#: ../xfburn/xfburn-burn-image-dialog.c:222
msgid "_Burn image"
msgstr "_Burn image"

#: ../xfburn/xfburn-burn-image-dialog.c:275
msgid "Burn mode is not currently implemented."
msgstr "Burn mode is not currently implemented."

#: ../xfburn/xfburn-burn-image-dialog.c:326
#: ../xfburn/xfburn-burn-image-dialog.c:355
msgid "An error occurred in the burn backend"
msgstr "An error occurred in the burn backend"

#: ../xfburn/xfburn-burn-image-dialog.c:339
msgid "Unable to determine image size."
msgstr "Unable to determine image size."

#: ../xfburn/xfburn-burn-image-dialog.c:346
msgid "Cannot open image."
msgstr "Cannot open image."

#: ../xfburn/xfburn-burn-image-dialog.c:383
msgid "Burning image..."
msgstr "Burning image..."

#: ../xfburn/xfburn-burn-image-dialog.c:456
msgid ""
"<span weight=\"bold\" foreground=\"darkred\" stretch=\"semiexpanded\">Please"
" select an image to burn</span>"
msgstr "<span weight=\"bold\" foreground=\"darkred\" stretch=\"semiexpanded\">Please select an image to burn</span>"

#: ../xfburn/xfburn-burn-image-dialog.c:496
msgid ""
"Cannot append data to multisession disc in this write mode (use TAO instead)"
msgstr "Cannot append data to multisession disc in this write mode (use TAO instead)"

#: ../xfburn/xfburn-burn-image-dialog.c:500
msgid "Closed disc with data detected. Need blank or appendable disc"
msgstr "Closed disc with data detected. Need blank or appendable disc"

#: ../xfburn/xfburn-burn-image-dialog.c:502
msgid "No disc detected in drive"
msgstr "No disc detected in drive"

#: ../xfburn/xfburn-burn-image-dialog.c:504
msgid "Cannot recognize state of drive and disc"
msgstr "Cannot recognise state of drive and disc"

#: ../xfburn/xfburn-burn-image-dialog.c:516
msgid "The selected image does not fit on the inserted disc"
msgstr "The selected image does not fit on the inserted disc"

#: ../xfburn/xfburn-burn-image-dialog.c:520
msgid "Failed to get image size"
msgstr "Failed to get image size"

#: ../xfburn/xfburn-burn-image-dialog.c:549
msgid ""
"Make sure you selected a valid file and you have the proper permissions to "
"access it."
msgstr "Make sure you selected a valid file and you have the proper permissions to access it."

#: ../xfburn/xfburn-compositions-notebook.c:147
msgid "Audio composition"
msgstr "Audio composition"

#: ../xfburn/xfburn-compositions-notebook.c:208
msgid "Welcome"
msgstr "Welcome"

#: ../xfburn/xfburn-copy-cd-dialog.c:88
#: ../xfburn/xfburn-copy-cd-progress-dialog.c:156
msgid "Copy data CD"
msgstr "Copy data CD"

#: ../xfburn/xfburn-copy-cd-dialog.c:99
msgid "CD Reader device"
msgstr "CD Reader device"

#: ../xfburn/xfburn-copy-cd-dialog.c:128
#: ../xfburn/xfburn-copy-dvd-dialog.c:125
msgid "On the _fly"
msgstr "On the _fly"

#: ../xfburn/xfburn-copy-cd-dialog.c:169
msgid "_Copy CD"
msgstr "_Copy CD"

#: ../xfburn/xfburn-copy-cd-progress-dialog.c:105
msgid "Please insert a recordable disc."
msgstr "Please insert a recordable disc."

#: ../xfburn/xfburn-copy-cd-progress-dialog.c:127
msgid "Writing CD..."
msgstr "Writing CD..."

#: ../xfburn/xfburn-copy-cd-progress-dialog.c:140
#: ../xfburn/xfburn-create-iso-progress-dialog.c:102
msgid "Reading CD..."
msgstr "Reading CD..."

#: ../xfburn/xfburn-copy-dvd-dialog.c:85
msgid "Copy data DVD"
msgstr "Copy data DVD"

#: ../xfburn/xfburn-copy-dvd-dialog.c:96
msgid "DVD Reader device"
msgstr "DVD Reader device"

#: ../xfburn/xfburn-copy-dvd-dialog.c:166
msgid "_Copy DVD"
msgstr "_Copy DVD"

#: ../xfburn/xfburn-create-iso-progress-dialog.c:117
msgid "Create ISO from CD"
msgstr "Create ISO from CD"

#: ../xfburn/xfburn-data-composition.c:339
#: ../xfburn/xfburn-audio-composition.c:216
#: ../xfburn/xfburn-audio-composition.c:366
msgid "Add the selected file(s) to the composition"
msgstr "Add the selected file(s) to the composition"

#: ../xfburn/xfburn-data-composition.c:341
msgid "Add a new directory to the composition"
msgstr "Add a new directory to the composition"

#: ../xfburn/xfburn-data-composition.c:346
#: ../xfburn/xfburn-audio-composition.c:218
#: ../xfburn/xfburn-audio-composition.c:371
msgid "Remove the selected file(s) from the composition"
msgstr "Remove the selected file(s) from the composition"

#: ../xfburn/xfburn-data-composition.c:348
#: ../xfburn/xfburn-audio-composition.c:220
#: ../xfburn/xfburn-audio-composition.c:373
msgid "Clear the content of the composition"
msgstr "Clear the content of the composition"

#: ../xfburn/xfburn-data-composition.c:359
msgid "Volume name :"
msgstr "Volume name :"

#: ../xfburn/xfburn-data-composition.c:393
msgid "Contents"
msgstr "Contents"

#: ../xfburn/xfburn-data-composition.c:408
#: ../xfburn/xfburn-directory-browser.c:113
msgid "Size"
msgstr "Size"

#: ../xfburn/xfburn-data-composition.c:411
msgid "Local Path"
msgstr "Local Path"

#: ../xfburn/xfburn-data-composition.c:688
msgid "You must give a name to the file."
msgstr "You must give a name to the file."

#: ../xfburn/xfburn-data-composition.c:697
#: ../xfburn/xfburn-data-composition.c:1214
#: ../xfburn/xfburn-audio-composition.c:1179
msgid "A file with the same name is already present in the composition."
msgstr "A file with the same name is already present in the composition."

#: ../xfburn/xfburn-data-composition.c:804
#: ../xfburn/xfburn-data-composition.c:805
#: ../xfburn/xfburn-data-composition.c:807
msgid "New directory"
msgstr "New directory"

#: ../xfburn/xfburn-data-composition.c:951
#: ../xfburn/xfburn-audio-composition.c:976
msgid "File(s) to add to composition"
msgstr "File(s) to add to composition"

#: ../xfburn/xfburn-data-composition.c:972
#: ../xfburn/xfburn-audio-composition.c:216
#: ../xfburn/xfburn-audio-composition.c:979
msgid "Add"
msgstr "Add"

#. Note to translators: first %s is the date in "i18n" format (year-month-
#. day), %d is a running number of compositions
#: ../xfburn/xfburn-data-composition.c:1057
#, c-format
msgid "Data %s~%d"
msgstr "Data %s~%d"

#: ../xfburn/xfburn-data-composition.c:1293
#, c-format
msgid ""
"%s cannot be added to the composition, because it exceeds the maximum "
"allowed file size for iso9660."
msgstr "%s cannot be added to the composition, because it exceeds the maximum allowed file size for iso9660."

#: ../xfburn/xfburn-data-composition.c:1300
#, c-format
msgid ""
"%s is larger than what iso9660 level 2 allows. This can be a problem for old"
" systems or software."
msgstr "%s is larger than what iso9660 level 2 allows. This can be a problem for old systems or software."

#: ../xfburn/xfburn-data-composition.c:1384
#: ../xfburn/xfburn-audio-composition.c:1355
msgid "Adding home directory"
msgstr "Adding home directory"

#: ../xfburn/xfburn-data-composition.c:1385
#: ../xfburn/xfburn-audio-composition.c:1356
msgid ""
"You are about to add your home directory to the composition. This is likely to take a very long time, and also to be too big to fit on one disc.\n"
"\n"
"Are you sure you want to proceed?"
msgstr "You are about to add your home directory to the composition. This is likely to take a very long time, and also to be too big to fit on one disc.\n\nAre you sure you want to proceed?"

#: ../xfburn/xfburn-data-composition.c:1553
#, c-format
msgid ""
"A file named \"%s\" already exists in this directory, the file hasn't been "
"added."
msgstr "A file named \"%s\" already exists in this directory, the file hasn't been added."

#: ../xfburn/xfburn-data-composition.c:1952
#, c-format
msgid "%s: null pointer"
msgstr "%s: null pointer"

#: ../xfburn/xfburn-data-composition.c:1954
#, c-format
msgid "%s: out of memory"
msgstr "%s: out of memory"

#: ../xfburn/xfburn-data-composition.c:1956
#, c-format
msgid "%s: node name not unique"
msgstr "%s: node name not unique"

#: ../xfburn/xfburn-data-composition.c:1958
#, c-format
msgid "%s: %s (code %X)"
msgstr "%s: %s (code %X)"

#. The first string is the renamed name, the second one the original name
#: ../xfburn/xfburn-data-composition.c:1978
#, c-format
msgid "Duplicate filename '%s' for '%s'"
msgstr "Duplicate filename '%s' for '%s'"

#: ../xfburn/xfburn-data-composition.c:2043
msgid "Error(s) occured while adding files"
msgstr "Error(s) occured while adding files"

#: ../xfburn/xfburn-data-composition.c:2047
msgid "OK"
msgstr "OK"

#: ../xfburn/xfburn-device-box.c:140 ../xfburn/xfburn-device-box.c:141
msgid "Show writers only"
msgstr "Show writers only"

#: ../xfburn/xfburn-device-box.c:144
msgid "Show speed selection"
msgstr "Show speed selection"

#: ../xfburn/xfburn-device-box.c:145
msgid "Show speed selection combo"
msgstr "Show speed selection combo"

#: ../xfburn/xfburn-device-box.c:148
msgid "Show mode selection"
msgstr "Show mode selection"

#: ../xfburn/xfburn-device-box.c:149
msgid "Show mode selection combo"
msgstr "Show mode selection combo"

#: ../xfburn/xfburn-device-box.c:152
msgid "Is it a valid combination"
msgstr "Is it a valid combination"

#: ../xfburn/xfburn-device-box.c:153
msgid "Is the combination of hardware and disc valid to burn the composition?"
msgstr "Is the combination of hardware and disc valid to burn the composition?"

#: ../xfburn/xfburn-device-box.c:157
msgid "The blank mode shows different disc status messages than regular mode"
msgstr "The blank mode shows different disc status messages than regular mode"

#: ../xfburn/xfburn-device-box.c:206
msgid "_Speed:"
msgstr "_Speed:"

#: ../xfburn/xfburn-device-box.c:229
msgid "Write _mode:"
msgstr "Write _mode:"

#: ../xfburn/xfburn-device-box.c:340
msgid "Empty speed list"
msgstr "Empty speed list"

#: ../xfburn/xfburn-device-box.c:349
msgid ""
"<b>Unable to retrieve the speed list for the drive.</b>\n"
"\n"
"This is a known bug for drives. If you're interested in fixing it, please have a look at the libburn library.\n"
"\n"
"Burning should still work, but if there are problems anyways, please let us know.\n"
"\n"
"<i>Thank you!</i>"
msgstr "<b>Unable to retrieve the speed list for the drive.</b>\n\nThis is a known bug, which occurs with some drives. Please report it to <i>xfburn@xfce.org</i> together with the console output to increase the chances that it will get fixed.\n\nBurning should still work, but if there are problems anyways, please let us know.\n\n<i>Thank you!</i>"

#: ../xfburn/xfburn-device-box.c:360
msgid "Continue to _show this notice"
msgstr "Continue to _show this notice"

#: ../xfburn/xfburn-device-box.c:407
msgid "default"
msgstr "default"

#: ../xfburn/xfburn-device-box.c:438
msgid "Max"
msgstr "Max"

#: ../xfburn/xfburn-device-box.c:473
msgid "A full, but erasable disc is in the drive"
msgstr "A full, but erasable disc is in the drive"

#: ../xfburn/xfburn-device-box.c:474
msgid ""
"Do you want to blank the disc, so that it can be used for the upcoming burn "
"process?"
msgstr "Do you want to blank the disc, so that it can be used for the upcoming burn process?"

#: ../xfburn/xfburn-device-box.c:545
msgid "Drive can't burn on the inserted disc"
msgstr "Drive can't burn to the inserted disc"

#: ../xfburn/xfburn-device-box.c:554 ../xfburn/xfburn-device-box.c:600
msgid "Drive is empty"
msgstr "Drive is empty"

#: ../xfburn/xfburn-device-box.c:557
msgid "Sorry, multisession is not yet supported"
msgstr "Sorry, multisession is not yet supported"

#: ../xfburn/xfburn-device-box.c:560
msgid "Inserted disc is full"
msgstr "Inserted disc is full"

#: ../xfburn/xfburn-device-box.c:563 ../xfburn/xfburn-device-box.c:606
msgid "Inserted disc is unsuitable"
msgstr "Inserted disc is unsuitable"

#: ../xfburn/xfburn-device-box.c:566 ../xfburn/xfburn-device-box.c:609
msgid "Cannot access drive (it might be in use)"
msgstr "Cannot access drive (it might be in use)"

#: ../xfburn/xfburn-device-box.c:571 ../xfburn/xfburn-device-box.c:612
msgid "Error determining disc"
msgstr "Error determining disc"

#: ../xfburn/xfburn-device-box.c:592
msgid "Write-once disc, no blanking possible"
msgstr "Write-once disc, no blanking possible"

#: ../xfburn/xfburn-device-box.c:595
msgid "DVD+RW does not need blanking"
msgstr "DVD+RW does not need blanking"

#: ../xfburn/xfburn-device-box.c:603
msgid "Inserted disc is already blank"
msgstr "Inserted disc is already blank"

#: ../xfburn/xfburn-device-box.c:639
msgid "Auto"
msgstr "Auto"

#: ../xfburn/xfburn-device-list.c:204 ../xfburn/xfburn-device-list.c:205
msgid "Number of burners in the system"
msgstr "Number of burners in the system"

#: ../xfburn/xfburn-device-list.c:207
msgid "Number of drives in the system"
msgstr "Number of drives in the system"

#: ../xfburn/xfburn-device-list.c:208
msgid "Number of drives in the system (readers and writers)"
msgstr "Number of drives in the system (readers and writers)"

#: ../xfburn/xfburn-device-list.c:210 ../xfburn/xfburn-device-list.c:211
msgid "List of devices"
msgstr "List of devices"

#: ../xfburn/xfburn-device-list.c:213 ../xfburn/xfburn-device-list.c:214
msgid "Currently selected device"
msgstr "Currently selected device"

#. globals
#: ../xfburn/xfburn-directory-browser.c:52
msgid "Folder"
msgstr "Folder"

#: ../xfburn/xfburn-directory-browser.c:100
msgid "File"
msgstr "File"

#: ../xfburn/xfburn-directory-browser.c:115
msgid "Type"
msgstr "Type"

#: ../xfburn/xfburn-fs-browser.c:96 ../xfburn/xfburn-fs-browser.c:305
msgid "Filesystem"
msgstr "Filesystem"

#. load the user's home dir
#: ../xfburn/xfburn-fs-browser.c:284
#, c-format
msgid "%s's home"
msgstr "%s's home"

#: ../xfburn/xfburn-main.c:202 ../xfburn.desktop.in.h:1
msgid "Xfburn"
msgstr "Xfburn"

#: ../xfburn/xfburn-main.c:211
#, c-format
msgid ""
"%s: %s\n"
"Try %s --help to see a full list of available command line options.\n"
msgstr "%s: %s\nTry %s --help to see a full list of available command line options.\n"

#: ../xfburn/xfburn-main.c:220
msgid "Unable to initialize the burning backend."
msgstr "Unable to initialise the burning backend."

#: ../xfburn/xfburn-main.c:294
msgid "No burners are currently available"
msgstr "No burners are currently available"

#: ../xfburn/xfburn-main.c:296
msgid ""
"Possibly the disc(s) are in use, and cannot get accessed.\n"
"\n"
"Please unmount and restart the application.\n"
"\n"
"If no disc is in the drive, check that you have read and write access to the drive with the current user."
msgstr "Possibly the disc(s) are in use, and cannot get accessed.\n\nPlease unmount and restart the application.\n\nIf no disc is in the drive, check that you have read and write access to the drive with the current user."

#: ../xfburn/xfburn-main.c:330
#, c-format
msgid ""
"Failed to initialize %s transcoder: %s\n"
"\t(falling back to basic implementation)"
msgstr "Failed to initialize %s transcoder: %s\n\t(falling back to basic implementation)"

#: ../xfburn/xfburn-main.c:373
#, c-format
msgid "Image file '%s' does not exist."
msgstr "Image file '%s' does not exist."

#: ../xfburn/xfburn-main-window.c:573
msgid "Another cd burning GUI"
msgstr "Another cd burning GUI"

#: ../xfburn/xfburn-notebook-tab.c:81
msgid "Label"
msgstr "Label"

#: ../xfburn/xfburn-notebook-tab.c:81
msgid "The text of the label"
msgstr "The text of the label"

#: ../xfburn/xfburn-notebook-tab.c:84
msgid "Show close button"
msgstr "Show close button"

#: ../xfburn/xfburn-notebook-tab.c:84
msgid "Determine whether the close button is visible"
msgstr "Determine whether the close button is visible"

#: ../xfburn/xfburn-perform-burn.c:103 ../xfburn/xfburn-perform-burn.c:452
msgid "Formatting..."
msgstr "Formatting..."

#: ../xfburn/xfburn-perform-burn.c:270
msgid ""
"Cannot append data to multisession disc in this write mode (use TAO "
"instead)."
msgstr "Cannot append data to multisession disc in this write mode (use TAO instead)."

#: ../xfburn/xfburn-perform-burn.c:274
msgid "Closed disc with data detected, a blank or appendable disc is needed."
msgstr "Closed disc with data detected, a blank or appendable disc is needed."

#: ../xfburn/xfburn-perform-burn.c:276
msgid "No disc detected in drive."
msgstr "No disc detected in drive."

#: ../xfburn/xfburn-perform-burn.c:279
msgid "Cannot recognize the state of the drive and disc."
msgstr "Cannot recognise the state of the drive and disc."

#: ../xfburn/xfburn-perform-burn.c:286
msgid "Formatting failed."
msgstr "Formatting failed."

#: ../xfburn/xfburn-perform-burn.c:304
msgid "There is not enough space available on the inserted disc."
msgstr "There is not enough space available on the inserted disc."

#: ../xfburn/xfburn-perform-burn.c:349
#, c-format
msgid "Burning track %2d/%d..."
msgstr "Burning track %2d/%d..."

#: ../xfburn/xfburn-perform-burn.c:353 ../xfburn/xfburn-perform-burn.c:417
msgid "Burning composition..."
msgstr "Burning composition..."

#: ../xfburn/xfburn-perform-burn.c:386
msgid "standby"
msgstr "standby"

#: ../xfburn/xfburn-perform-burn.c:394
msgid "ending"
msgstr "ending"

#: ../xfburn/xfburn-perform-burn.c:397
msgid "failing"
msgstr "failing"

#: ../xfburn/xfburn-perform-burn.c:400
msgid "unused"
msgstr "unused"

#: ../xfburn/xfburn-perform-burn.c:403
msgid "abandoned"
msgstr "abandoned"

#: ../xfburn/xfburn-perform-burn.c:406
msgid "ended"
msgstr "ended"

#: ../xfburn/xfburn-perform-burn.c:409
msgid "aborted"
msgstr "aborted"

#: ../xfburn/xfburn-perform-burn.c:412
msgid "no info"
msgstr "no info"

#: ../xfburn/xfburn-perform-burn.c:422
msgid "Writing Lead-In..."
msgstr "Writing Lead-In..."

#: ../xfburn/xfburn-perform-burn.c:428
msgid "Writing Lead-Out..."
msgstr "Writing Lead-Out..."

#: ../xfburn/xfburn-perform-burn.c:434
msgid "Writing pregap..."
msgstr "Writing pregap..."

#: ../xfburn/xfburn-perform-burn.c:440
msgid "Closing track..."
msgstr "Closing track..."

#: ../xfburn/xfburn-perform-burn.c:446
msgid "Closing session..."
msgstr "Closing session..."

#: ../xfburn/xfburn-perform-burn.c:465
msgid "see console"
msgstr "see console"

#: ../xfburn/xfburn-perform-burn.c:493
msgid "User Aborted"
msgstr "User Aborted"

#: ../xfburn/xfburn-preferences-dialog.c:116
msgid "Preferences"
msgstr "Preferences"

#: ../xfburn/xfburn-preferences-dialog.c:117
msgid "Tune how Xfburn behaves"
msgstr "Tune how Xfburn behaves"

#: ../xfburn/xfburn-preferences-dialog.c:162
#: ../xfburn/xfburn-preferences-dialog.c:166
msgid "Temporary directory"
msgstr "Temporary directory"

#: ../xfburn/xfburn-preferences-dialog.c:170
msgid "_Clean temporary directory on exit"
msgstr "_Clean temporary directory on exit"

#: ../xfburn/xfburn-preferences-dialog.c:177
msgid "File browser"
msgstr "File browser"

#: ../xfburn/xfburn-preferences-dialog.c:181
msgid "Show _hidden files"
msgstr "Show _hidden files"

#: ../xfburn/xfburn-preferences-dialog.c:186
msgid "Show human_readable filesizes"
msgstr "Show human_readable filesizes"

#: ../xfburn/xfburn-preferences-dialog.c:196
#: ../xfburn/xfburn-preferences-dialog.c:207
msgid "General"
msgstr "General"

#: ../xfburn/xfburn-preferences-dialog.c:211
#: ../xfburn/xfburn-preferences-dialog.c:292
msgid "Devices"
msgstr "Devices"

#: ../xfburn/xfburn-preferences-dialog.c:220
msgid "Detected devices"
msgstr "Detected devices"

#: ../xfburn/xfburn-preferences-dialog.c:242
msgid "Name"
msgstr "Name"

#: ../xfburn/xfburn-preferences-dialog.c:255
msgid "Node"
msgstr "Node"

#: ../xfburn/xfburn-preferences-dialog.c:257
msgid "Write CD-R"
msgstr "Write CD-R"

#: ../xfburn/xfburn-preferences-dialog.c:259
msgid "Write CD-RW"
msgstr "Write CD-RW"

#: ../xfburn/xfburn-preferences-dialog.c:262
msgid "Write DVD-R"
msgstr "Write DVD-R"

#: ../xfburn/xfburn-preferences-dialog.c:265
msgid "Write DVD-RAM"
msgstr "Write DVD-RAM"

#: ../xfburn/xfburn-preferences-dialog.c:268
msgid "Write Blu-ray"
msgstr "Write Blu-ray"

#: ../xfburn/xfburn-preferences-dialog.c:276
msgid "Sc_an for devices"
msgstr "Sc_an for devices"

#: ../xfburn/xfburn-preferences-dialog.c:303
msgid "Show warning on _empty speed list"
msgstr "Show warning on _empty speed list"

#: ../xfburn/xfburn-preferences-dialog.c:312
msgid "FIFO buffer size (in kb)"
msgstr "FIFO buffer size (in kb)"

#: ../xfburn/xfburn-preferences-dialog.c:464
msgid "Changing this setting only takes full effect after a program restart."
msgstr "Changing this setting only takes full effect after a program restart."

#: ../xfburn/xfburn-progress-dialog.c:172
#: ../xfburn/xfburn-progress-dialog.c:527
msgid "Initializing..."
msgstr "Initialising..."

#: ../xfburn/xfburn-progress-dialog.c:187
msgid "Estimated writing speed:"
msgstr "Estimated writing speed:"

#: ../xfburn/xfburn-progress-dialog.c:190
#: ../xfburn/xfburn-progress-dialog.c:206
#: ../xfburn/xfburn-progress-dialog.c:215
#: ../xfburn/xfburn-progress-dialog.c:306
#: ../xfburn/xfburn-progress-dialog.c:450
#: ../xfburn/xfburn-progress-dialog.c:474
#: ../xfburn/xfburn-progress-dialog.c:498
msgid "unknown"
msgstr "unknown"

#: ../xfburn/xfburn-progress-dialog.c:201
msgid "FIFO buffer:"
msgstr "FIFO buffer:"

#: ../xfburn/xfburn-progress-dialog.c:210
msgid "Device buffer:"
msgstr "Device buffer:"

#. action buttons
#: ../xfburn/xfburn-progress-dialog.c:220
msgid "_Stop"
msgstr "_Stop"

#: ../xfburn/xfburn-progress-dialog.c:348
msgid "Are you sure you want to abort?"
msgstr "Are you sure you want to abort?"

#: ../xfburn/xfburn-progress-dialog.c:476
#, c-format
msgid "Min. fill was %2d%%"
msgstr "Min. fill was %2d%%"

#: ../xfburn/xfburn-progress-dialog.c:544
msgid "Aborted"
msgstr "Aborted"

#: ../xfburn/xfburn-progress-dialog.c:547
msgid "Formatted."
msgstr "Formatted."

#: ../xfburn/xfburn-progress-dialog.c:556
msgid "Failed"
msgstr "Failed"

#: ../xfburn/xfburn-progress-dialog.c:559
msgid "Cancelled"
msgstr "Cancelled"

#: ../xfburn/xfburn-progress-dialog.c:562
msgid "Completed"
msgstr "Completed"

#: ../xfburn/xfburn-progress-dialog.c:600
msgid "Aborting..."
msgstr "Aborting..."

#: ../xfburn/xfburn-utils.c:131
msgid "Select command"
msgstr "Select command"

#: ../xfburn/xfburn-welcome-tab.c:99
msgid "Welcome to Xfburn!"
msgstr "Welcome to Xfburn!"

#. buttons
#: ../xfburn/xfburn-welcome-tab.c:118
msgid "<big>Burn _Image</big>"
msgstr "<big>Burn _Image</big>"

#: ../xfburn/xfburn-welcome-tab.c:118
msgid "Burn a prepared compilation, i.e. an .ISO file"
msgstr "Burn a prepared compilation, i.e. an .ISO file"

#: ../xfburn/xfburn-welcome-tab.c:123
msgid "<big>New _Data Composition</big>"
msgstr "<big>New _Data Composition</big>"

#: ../xfburn/xfburn-welcome-tab.c:123
msgid "Create a new data disc with the files of your choosing"
msgstr "Create a new data disc with the files of your choosing"

#: ../xfburn/xfburn-welcome-tab.c:128
msgid "<big>_Blank Disc</big>"
msgstr "<big>_Blank Disc</big>"

#: ../xfburn/xfburn-welcome-tab.c:128
msgid "Prepare the rewriteable disc for a new burn"
msgstr "Prepare the rewriteable disc for a new burn"

#: ../xfburn/xfburn-welcome-tab.c:133
msgid "<big>_Audio CD</big>"
msgstr "<big>_Audio CD</big>"

#: ../xfburn/xfburn-welcome-tab.c:133
msgid "Audio CD playable in regular stereos"
msgstr "Audio CD playable in regular stereos"

#: ../xfburn/xfburn-audio-composition.c:218
msgid "Remove"
msgstr "Remove"

#: ../xfburn/xfburn-audio-composition.c:220
msgid "Clear"
msgstr "Clear"

#: ../xfburn/xfburn-audio-composition.c:222
#: ../xfburn/xfburn-audio-composition.c:379
msgid "What files can get burned to an audio CD?"
msgstr "What files can get burned to an audio CD?"

#: ../xfburn/xfburn-audio-composition.c:226
msgid "Rename Artist"
msgstr "Rename Artist"

#: ../xfburn/xfburn-audio-composition.c:226
msgid "Rename the artist of the selected file"
msgstr "Rename the artist of the selected file"

#: ../xfburn/xfburn-audio-composition.c:228
msgid "Rename Title"
msgstr "Rename Title"

#: ../xfburn/xfburn-audio-composition.c:228
msgid "Rename the title of the selected file"
msgstr "Rename the title of the selected file"

#: ../xfburn/xfburn-audio-composition.c:410
msgid "Pos"
msgstr "Pos"

#: ../xfburn/xfburn-audio-composition.c:412
msgid "Length"
msgstr "Length"

#: ../xfburn/xfburn-audio-composition.c:418
msgid "Artist"
msgstr "Artist"

#: ../xfburn/xfburn-audio-composition.c:431
msgid "Title"
msgstr "Title"

#: ../xfburn/xfburn-audio-composition.c:442
msgid "Filename"
msgstr "Filename"

#: ../xfburn/xfburn-audio-composition.c:589
msgid "Cannot burn audio onto a DVD."
msgstr "Cannot burn audio onto a DVD."

#: ../xfburn/xfburn-audio-composition.c:1263
msgid "You can only have a maximum of 99 tracks."
msgstr "You can only have a maximum of 99 tracks."

#: ../xfburn/xfburn-burn-audio-cd-composition-dialog.c:279
msgid "A problem with the burn backend occurred."
msgstr "A problem with the burn backend occurred."

#: ../xfburn/xfburn-disc-usage.c:161
msgid "Proceed to Burn"
msgstr "Proceed to Burn"

#: ../xfburn/xfburn-disc-usage.c:201
msgid "You are trying to burn more onto the disc than it can hold."
msgstr "You are trying to burn more onto the disc than it can hold."

#: ../xfburn/xfburn-transcoder-basic.c:129
msgid "basic"
msgstr "basic"

#: ../xfburn/xfburn-transcoder-basic.c:135
msgid ""
"The basic transcoder is built in,\n"
"and does not require any library.\n"
"But it can only handle uncompressed\n"
".wav files.\n"
"If you would like to create audio\n"
"compositions from different types of\n"
"audio files, please compile with\n"
"gstreamer support."
msgstr "The basic transcoder is built in,\nand does not require any library.\nBut it can only handle uncompressed\n.wav files.\nIf you would like to create audio\ncompositions from different types of\naudio files, please compile with\ngstreamer support."

#: ../xfburn/xfburn-transcoder-basic.c:159
#, c-format
msgid "File %s does not have a .wav extension"
msgstr "File %s does not have a .wav extension"

#: ../xfburn/xfburn-transcoder-basic.c:164
#, c-format
msgid "File %s does not contain uncompressed PCM wave audio"
msgstr "File %s does not contain uncompressed PCM wave audio"

#: ../xfburn/xfburn-transcoder-basic.c:170
#, c-format
msgid "Could not stat %s: %s"
msgstr "Could not stat %s: %s"

#: ../xfburn/xfburn-transcoder-basic.c:202
#, c-format
msgid "Could not open %s."
msgstr "Could not open %s."

#: ../xfburn/xfburn-transcoder-basic.c:281
#, c-format
msgid "Could not open %s: %s"
msgstr "Could not open %s: %s"

#. Note to translators: you can probably keep this as gstreamer,
#. * unless you have a good reason to call it by another name that
#. * the user would understand better
#: ../xfburn/xfburn-transcoder-gst.c:579
msgid "gstreamer"
msgstr "gstreamer"

#: ../xfburn/xfburn-transcoder-gst.c:585
msgid ""
"The gstreamer transcoder uses the gstreamer\n"
"library for creating audio compositions.\n"
"\n"
"Essentially all audio files should be supported\n"
"given that the correct plugins are installed.\n"
"If an audio file is not recognized, make sure\n"
"that you have the 'good','bad', and 'ugly'\n"
"gstreamer plugin packages installed."
msgstr "The gstreamer transcoder uses the gstreamer\nlibrary for creating audio compositions.\n\nEssentially all audio files should be supported\ngiven that the correct plugins are installed.\nIf an audio file is not recognised, make sure\nthat you have the 'good','bad', and 'ugly'\ngstreamer plugin packages installed."

#: ../xfburn/xfburn-transcoder-gst.c:662 ../xfburn/xfburn-transcoder-gst.c:692
#, c-format
msgid "An error occurred while identifying '%s' with gstreamer"
msgstr "An error occurred while identifying '%s' with gstreamer"

#: ../xfburn/xfburn-transcoder-gst.c:679
msgid "A plugin"
msgstr "A plugin"

#: ../xfburn/xfburn-transcoder-gst.c:814
#, c-format
msgid "Gstreamer did not want to start transcoding (timed out)"
msgstr "Gstreamer did not want to start transcoding (timed out)"

#: ../xfburn/xfburn-transcoder-gst.c:849
#, c-format
msgid "Failed to change songs while transcoding"
msgstr "Failed to change songs while transcoding"

#: ../xfburn/xfburn-transcoder.c:124 ../xfburn/xfburn-transcoder.c:151
#: ../xfburn/xfburn-transcoder.c:163
#, c-format
msgid "not implemented"
msgstr "not implemented"

#: ../xfburn/xfburn-device.c:247 ../xfburn/xfburn-device.c:248
msgid "Display name"
msgstr "Display name"

#: ../xfburn/xfburn-device.c:250 ../xfburn/xfburn-device.c:251
msgid "Device address"
msgstr "Device address"

#: ../xfburn/xfburn-device.c:253 ../xfburn/xfburn-device.c:254
msgid "Burn speeds supported by the device"
msgstr "Burn speeds supported by the device"

#: ../xfburn/xfburn-device.c:256 ../xfburn/xfburn-device.c:257
msgid "Disc status"
msgstr "Disc status"

#: ../xfburn/xfburn-device.c:259 ../xfburn/xfburn-device.c:260
msgid "Profile no. as reported by libburn"
msgstr "Profile no. as reported by libburn"

#: ../xfburn/xfburn-device.c:262 ../xfburn/xfburn-device.c:263
msgid "Profile name as reported by libburn"
msgstr "Profile name as reported by libburn"

#: ../xfburn/xfburn-device.c:265 ../xfburn/xfburn-device.c:266
msgid "Is the disc erasable"
msgstr "Is the disc erasable"

#: ../xfburn/xfburn-device.c:268 ../xfburn/xfburn-device.c:269
msgid "Can burn CDR"
msgstr "Can burn CDR"

#: ../xfburn/xfburn-device.c:271 ../xfburn/xfburn-device.c:272
msgid "Can burn CDRW"
msgstr "Can burn CDRW"

#: ../xfburn/xfburn-device.c:274 ../xfburn/xfburn-device.c:275
msgid "Can burn DVDR"
msgstr "Can burn DVDR"

#: ../xfburn/xfburn-device.c:277 ../xfburn/xfburn-device.c:278
msgid "Can burn DVDPLUSR"
msgstr "Can burn DVDPLUSR"

#: ../xfburn/xfburn-device.c:280 ../xfburn/xfburn-device.c:281
msgid "Can burn DVDRAM"
msgstr "Can burn DVDRAM"

#: ../xfburn/xfburn-device.c:283 ../xfburn/xfburn-device.c:284
msgid "Can burn Blu-ray"
msgstr "Can burn Blu-ray"

#: ../xfburn/xfburn-device.c:286 ../xfburn/xfburn-device.c:287
msgid "libburn TAO block types"
msgstr "libburn TAO block types"

#: ../xfburn/xfburn-device.c:289 ../xfburn/xfburn-device.c:290
msgid "libburn SAO block types"
msgstr "libburn SAO block types"

#: ../xfburn/xfburn-device.c:292 ../xfburn/xfburn-device.c:293
msgid "libburn RAW block types"
msgstr "libburn RAW block types"

#: ../xfburn/xfburn-device.c:295 ../xfburn/xfburn-device.c:296
msgid "libburn PACKET block types"
msgstr "libburn PACKET block types"

#: ../desktop-integration/thunar-sendto-xfburn.desktop.in.h:1
msgid "Data Composition"
msgstr "Data Composition"

#: ../xfburn.desktop.in.h:2
msgid "Disk Burning"
msgstr "Disk Burning"

#: ../xfburn.desktop.in.h:3
msgid "CD and DVD burning application"
msgstr "CD and DVD burning application"

#: ../xfburn.desktop.in.h:4
msgid "Burn Image (xfburn)"
msgstr "Burn Image (xfburn)"

#: ../xfburn.appdata.xml.in.h:1
msgid ""
"Xfburn is a simple CD/DVD burning tool based on libburnia libraries. It can "
"blank CD/DVD(-RW)s, burn and create iso images, audio CDs, as well as burn "
"personal compositions of data to either CD or DVD. It is stable and under "
"ongoing development."
msgstr "Xfburn is a simple CD/DVD burning tool based on libburnia libraries. It can blank CD/DVD(-RW)s, burn and create iso images, audio CDs, as well as burn personal compositions of data to either CD or DVD. It is stable and under ongoing development."
